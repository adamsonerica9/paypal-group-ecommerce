const buttonPayOption = {
   onClick: (data, action) => {
      const body = document.getElementsByTagName('body')[0];
      body.style.opacity = '0.8';
      body.style.pointerEvents = 'none';
      const amount = document.getElementById('amount').value;
      const errorMesage = document.getElementsByClassName('err')[0];
      if (!amount) {
         errorMesage.textContent = 'Amount is required';
         errorMesage.classList.remove('d-none');
         errorMesage.classList.add('d-block');
         return action.reject();
      } else if (isNaN(amount)) {
         errorMesage.textContent = 'Amount must be number';
         errorMesage.classList.remove('d-none');
         errorMesage.classList.add('d-block');
         return action.reject();
      } else {
         errorMesage.classList.remove('d-block');
         errorMesage.classList.add('d-none');
         return action.resolve();
      }
   },
   createOrder: (data, action) => {
      const amount = document.getElementById('amount').value;
      return action.order.create({
         purchase_units: [
            {
               amount: {
                  value: amount
               }
            }
         ]
      })
   },
   onApprove: (data, action) => {
      return action.order.capture().then((detail) => {
         const body = document.getElementsByTagName('body')[0];
         body.style.opacity = '1';
         body.style.pointerEvents = 'auto';   
         const {surname, given_name} = detail.payer.name;
         Swal.fire({
            text: `Thank for purchasing ${given_name} ${surname}`,
            icon: 'success',
            allowOutsideClick: false
         })
      })
   }
}

paypal.Buttons(buttonPayOption).render('#button-pay');